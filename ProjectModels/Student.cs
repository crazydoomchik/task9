﻿using System.ComponentModel.DataAnnotations;


namespace UniversityMVS.Models
{
    public class Student
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Can not be Null")]
        public string Name { get; set; }
        
        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
