﻿using System.Collections.Generic;
using UniversityMVS.Models;

namespace ProjectModels.Interface
{
    public interface ICourseRepository
    {
        IEnumerable<Course> GetAllCourses();
        
    }
}
