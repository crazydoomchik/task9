﻿using System.Collections.Generic;
using UniversityMVS.Models;

namespace ProjectModels.Interface
{
    public interface IStudentRepository
    {
        IEnumerable<Student> GetStudentFromGroup(int idGroup);
        void UpdateStudent(Student student);
        bool CheckStudentFromGroup(int id);
        Student GetStudent(int id);
    }
}
