﻿using System.Collections.Generic;
using UniversityMVS.Models;

namespace ProjectModels.Interface
{
    public interface IGroupRepository
    {
        IEnumerable<Group> GetGroupFromCourses(int idCourse);
        void UpdateGroup(Group group);
        Group GetGroup(int id);
        void DeleteGroup(int id);
    }
}
