﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace UniversityMVS.Models
{
    public class Group
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Can not be Null")]
        public string Name { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public ICollection<Student> Students { get; set; }
        public Group()
        {
            Students = new List<Student>();
        }
    }
}
