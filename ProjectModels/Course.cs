﻿using System.Collections.Generic;


namespace UniversityMVS.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Group> Groups { get; set; }
        public Course()
        {
            Groups = new List<Group>();
        }
    }
}
