﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniversityMVS.Controllers
{
    public class UserExceptionController:Controller
    {
        public IActionResult ExceptionGroup(string exception)
        {
            ViewBag.Message = exception;
            return View();
        }
    }
}
