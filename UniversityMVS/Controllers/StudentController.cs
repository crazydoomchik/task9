﻿using Microsoft.AspNetCore.Mvc;
using ProjectModels.Interface;
using System.Linq;
using UniversityMVS.Models;

namespace UniversityMVS.Controllers
{
    public class StudentController:Controller
    {
        private IStudentRepository _repository;

        public StudentController(IStudentRepository repository)
        {
            _repository = repository;
        }

        public IActionResult GetStudent(int id)
        {
            return View(_repository.GetStudentFromGroup(id).ToList());
        }
        [HttpGet]
        public IActionResult UpdateStudent(int id)
        {
            var item = _repository.GetStudent(id);

            return View(item);
        }

        [HttpPost]
        public IActionResult UpdateStudent(Student student)
        {
            _repository.UpdateStudent(student);

            return RedirectToAction("GetStudent", new { id = student.GroupId});
        }
    }
}
