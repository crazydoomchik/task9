﻿using Microsoft.AspNetCore.Mvc;
using ProjectModels.Interface;
using System.Linq;
using UniversityMVS.Models;

namespace UniversityMVS.Controllers
{
    public class GroupController:Controller
    {
        private IGroupRepository _repositoryGroup;
        private IStudentRepository _repositoryStudent;

        public GroupController(IGroupRepository groupRepository, IStudentRepository studentRepository)
        {
            _repositoryGroup = groupRepository;
            _repositoryStudent = studentRepository;
        }
        public IActionResult GetGroup(int id)
        {
            return View(_repositoryGroup.GetGroupFromCourses(id).ToList());
        }

        [HttpGet]
        public IActionResult UpdateGroup(int id)
        {
            var item = _repositoryGroup.GetGroup(id);

            return View(item);
        }
        [HttpPost]
        public IActionResult UpdateGroup(Group group)
        {
            _repositoryGroup.UpdateGroup(group);

            return RedirectToAction("GetGroup", new {id = group.CourseId});
        }

        public IActionResult DeleteGroup(int id)
        {
            if (_repositoryStudent.CheckStudentFromGroup(id))
            {
                var exceptionText = "The group has students, group cannot be deleted!";
                return RedirectToAction("ExceptionGroup", "UserException", new {exception = exceptionText });
            }
            var item = _repositoryGroup.GetGroup(id);

            _repositoryGroup.DeleteGroup(id);
            return RedirectToAction("GetGroup", new { id = item.CourseId });
        }

        public IActionResult ExceptionDelete()
        {
            return View();
        }
    }
}
