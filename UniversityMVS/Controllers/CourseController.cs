﻿using Microsoft.AspNetCore.Mvc;
using ProjectModels.Interface;
using System.Linq;


namespace UniversityMVS.Controllers
{
    public class CourseController:Controller
    {
        private ICourseRepository _repository;
        public CourseController(ICourseRepository repository)
        {
            _repository = repository;
        }

        public IActionResult GetCourses()
        {

            return View(_repository.GetAllCourses().ToList());
        }
    }
}
