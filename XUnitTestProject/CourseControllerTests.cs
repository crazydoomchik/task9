﻿using Moq;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using ProjectModels.Interface;
using UniversityMVS.Models;
using UniversityMVS.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace XUnitTestProject
{
    public class CourseControllerTests
    {
        [Fact]
        public void GetCoursesTest()
        {
            //Arrange
            var mock = new Mock<ICourseRepository>();
            mock.Setup(rep => rep.GetAllCourses()).Returns(GetTestCourses());
            var controller = new CourseController(mock.Object);

            //Act
            var result = controller.GetCourses();

            //Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Course>>(viewResult.Model);
            Assert.Equal(GetTestCourses().Count, model.Count());

        }
        
        private List<Course> GetTestCourses()
        {
            var courses = new List<Course>
            {
                new Course{Id=1, Name ="c#"},
                new Course{Id=2, Name ="Java"},
                new Course{Id=3, Name ="JS"}
            };
            return courses;
        }
    }
}
