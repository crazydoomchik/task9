﻿using System.Collections.Generic;
using Xunit;
using Moq;
using UniversityMVS.Models;
using ProjectModels.Interface;
using System.Linq;
using UniversityMVS.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace XUnitTestProject
{
    public class GroupControllerTests
    {
        [Fact]
        public void GetGroupTest()
        {
            int idGroupOnCourses = 1;
            var mock = new Mock<IGroupRepository>();
            mock.Setup(rep => rep.GetGroupFromCourses(idGroupOnCourses))
                .Returns(GetTestGroups()
                .Where(g => g.CourseId == idGroupOnCourses));

            var controller = new GroupController(mock.Object);
            var result = controller.GetGroup(idGroupOnCourses);

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Group>>(viewResult.Model);
            Assert.Equal(2, model.Count());
        }       
        private List<Group> GetTestGroups()
        {
            var item = new List<Group>
            {
                new Group
                    {
                        Id=1, 
                        Name = "Junior c#",
                        CourseId = 1
                    },
                    new Group
                    {
                        Id=2,
                        Name = "Middle c#",
                        CourseId = 1
                    },
                    new Group
                    {
                        Id=3,
                        Name = "Junior Java",
                        CourseId = 2
                    },
                    new Group
                    {
                        Id=4,
                        Name = "Middle Java",
                        CourseId = 2
                    }
            };
            return item;
        }

        [Fact]
        public void UpdateGroupTest()
        {
            var mock = new Mock<IGroupRepository>();
            
            var controller = new GroupController(mock.Object);
            var newGroup = new Group() { Name = "CC", Id = 1 };
            mock.Setup(rep => rep.GetGroup(newGroup.Id))
                .Returns(GetTestGroups()
                .Where(g=>g.Id == newGroup.Id)
                .FirstOrDefault());

            var result = controller.UpdateGroup(newGroup);

            var redirect = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("GetGroup", redirect.ActionName);
            mock.Verify(r => r.UpdateGroup(newGroup));
        }

        [Fact]
        public void DeleteGroupTest()
        {
            int idDeleteGroup = 1;
            var mock = new Mock<IGroupRepository>();
            mock.Setup(rep => rep.CheckStudentFromGroup(idDeleteGroup))
                .Returns(true);

            var controller = new GroupController(mock.Object);

            var result = controller.DeleteGroup(idDeleteGroup);

            var redirect = Assert.IsType<RedirectToActionResult>(result);
                
        }

        
    }
}
