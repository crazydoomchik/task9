﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using ProjectModels.Interface;
using System.Collections.Generic;
using System.Linq;
using UniversityMVS.Controllers;
using UniversityMVS.Models;
using Xunit;

namespace XUnitTestProject
{
    public class StudentControllerTests
    {
        [Fact]
        public void GetStudentTest()
        {
            int idStudent = 1;
            var mock = new Mock<IStudentRepository>();
            mock.Setup(rep => rep.GetStudentFromGroup(idStudent))
                .Returns(GetTestStudent()
                .Where(s => s.GroupId == idStudent));

            var controller = new StudentController(mock.Object);

            var result = controller.GetStudent(idStudent);
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Student>>(viewResult.Model);
            Assert.Equal(2, model.Count());

        }

        private List<Student> GetTestStudent()
        {
            var item = new List<Student>
            {
                new Student
                    {
                        Id =1,
                        Name = "qqq",
                        GroupId = 1
                    },
                    new Student
                    {
                        Id =2,
                        Name = "www",
                        GroupId = 1
                    },
                    new Student
                    {
                        Id =3,
                        Name = "eee",
                        GroupId = 2
                    },
                    new Student
                    {
                        Id =4,
                        Name = "rrr",
                        GroupId = 2
                    },
                    new Student
                    {
                        Id =5,
                        Name = "aaa",
                        GroupId = 3
                    },
                    new Student
                    {
                        Id =6,
                        Name = "zzz",
                        GroupId = 3
                    },
                    new Student
                    {
                        Id =7,
                        Name = "xxx",
                        GroupId = 4
                    },
                    new Student
                    {
                        Id =8,
                        Name = "vvv",
                        GroupId = 4
                    }
            };
            return item;
        }

        [Fact]
        public void UpdateStudentTest()
        {
            var mock = new Mock<IStudentRepository>();

            var controller = new StudentController(mock.Object);
            var newStudent = new Student() { Name = "CC", Id = 1 };
            mock.Setup(rep => rep.GetStudent(newStudent.Id))
                .Returns(GetTestStudent()
                .Where(g => g.Id == newStudent.Id)
                .FirstOrDefault());

            var result = controller.UpdateStudent(newStudent);

            var redirect = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("GetStudent", redirect.ActionName);
            mock.Verify(r => r.UpdateStudent(newStudent));
        }
    }
}
