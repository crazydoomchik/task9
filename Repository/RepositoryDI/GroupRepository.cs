﻿using Microsoft.EntityFrameworkCore;
using ProjectModels.Interface;
using System.Collections.Generic;
using System.Linq;
using UniversityMVS.Models;

namespace Repository.RepositoryDI
{
    public class GroupRepository : IGroupRepository
    {
        private ApplicationContext _context;
        public GroupRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<Group> GetGroupFromCourses(int idCourse)
        {
            return _context.Groups.Where(g => g.CourseId == idCourse).Include(x=>x.Course);
        }

        public Group GetGroup(int id)
        {
            var item = _context.Groups.Where(x => x.Id == id).FirstOrDefault();
            return item;
        }

        public void UpdateGroup(Group group)
        {
            var item = _context.Groups.Where(g => g.Id == group.Id).FirstOrDefault();
            item.Name = group.Name;           
            _context.Groups.Update(item);
            _context.SaveChanges();           
        }

        public void DeleteGroup(int id)
        {
            var item = _context.Groups.Where(g => g.Id == id).FirstOrDefault();
            _context.Remove(item);
        }     

    }
}
