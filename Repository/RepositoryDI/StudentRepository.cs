﻿using Microsoft.EntityFrameworkCore;
using ProjectModels.Interface;
using System.Collections.Generic;
using System.Linq;
using UniversityMVS.Models;

namespace Repository.RepositoryDI
{
    public class StudentRepository : IStudentRepository
    {
        private ApplicationContext _context;
        public StudentRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<Student> GetStudentFromGroup(int idGroup)
        {
            return _context.Students.Where(s => s.GroupId == idGroup).Include(i=>i.Group);
        }

        public Student GetStudent(int id)
        {
            var item = _context.Students.Where(x => x.Id == id).FirstOrDefault();
            return item;
        }

        public void UpdateStudent(Student student)
        {
            var item = _context.Students.Where(s => s.Id == student.Id).FirstOrDefault();
            item.Name = student.Name;
            _context.Students.Update(item);
            _context.SaveChanges();
        }
        public bool CheckStudentFromGroup(int id)
        {
            return _context.Students.Where(s => s.GroupId == id).Any();
        }
    }
}
