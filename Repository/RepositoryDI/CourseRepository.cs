﻿using ProjectModels.Interface;
using System.Collections.Generic;
using UniversityMVS.Models;

namespace Repository.RepositoryDI
{
    public class CourseRepository : ICourseRepository
    {
        private ApplicationContext _context;

        public CourseRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<Course> GetAllCourses()
        {
            return _context.Courses;
        }
    }
}
