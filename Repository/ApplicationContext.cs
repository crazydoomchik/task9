﻿using Microsoft.EntityFrameworkCore;
using UniversityMVS.Models;

namespace Repository
{
    public class ApplicationContext:DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }

        public ApplicationContext (DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
