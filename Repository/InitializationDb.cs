﻿
using System.Linq;
using UniversityMVS.Models;

namespace Repository
{
    public static class InitializationDb
    {
        public static void Initialize(ApplicationContext context)
        {
            if (!context.Courses.Any())
            {
                context.Courses.AddRange(
                    new Course 
                    { 
                        Name ="c#"
                    },
                    new Course
                    {
                        Name ="Java"
                    }
                    );
                context.SaveChanges();
            }

            if (!context.Groups.Any())
            {
                context.Groups.AddRange(
                    new Group
                    {
                        Name = "Junior c#",
                        CourseId = 1
                    },
                    new Group
                    {
                        Name = "Middle c#",
                        CourseId = 2
                    },
                    new Group
                    {
                        Name = "Junior Java",
                        CourseId = 2
                    },
                    new Group
                    {
                        Name = "Middle Java",
                        CourseId = 2
                    }
                    );
                context.SaveChanges();
            }

            if (!context.Students.Any())
            {
                context.Students.AddRange(
                    new Student
                    {
                        Name = "qqq",
                        GroupId = 1
                    },
                    new Student
                    {
                        Name = "www",
                        GroupId = 1
                    },
                    new Student
                    {
                        Name = "eee",
                        GroupId = 2
                    },
                    new Student
                    {
                        Name = "rrr",
                        GroupId = 2
                    },
                    new Student
                    {
                        Name = "aaa",
                        GroupId = 3
                    },
                    new Student
                    {
                        Name = "zzz",
                        GroupId = 3
                    },
                    new Student
                    {
                        Name = "xxx",
                        GroupId = 4
                    },
                    new Student
                    {
                        Name = "vvv",
                        GroupId = 4
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}
